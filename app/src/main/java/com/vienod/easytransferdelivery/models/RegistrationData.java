package com.vienod.easytransferdelivery.models;

public class RegistrationData {

//    private String userTypeDesc = "";
    private int userTypeId = 0;
    private String name = "";
    private String nric = "";
    private String phone_num = "";
    private String email = "";
    private String username = "";
    private String password = "";

    public RegistrationData() {
    }

//    public String getUserTypeDesc() {
//        return userTypeDesc;
//    }
//
//    public void setUserTypeDesc(String userTypeDesc) {
//        this.userTypeDesc = userTypeDesc;
//    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNric() {
        return nric;
    }

    public void setNric(String nric) {
        this.nric = nric;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
