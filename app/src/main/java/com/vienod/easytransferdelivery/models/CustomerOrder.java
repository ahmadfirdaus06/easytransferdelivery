package com.vienod.easytransferdelivery.models;

public class CustomerOrder {

    private String id = "";
    private String updatedAt = "";
    private String createdAt = "";
    private String pickupDateTime = "";
    private String dropoffDateTime = "";
    private String pickupLocation = "";
    private String dropoffLocation = "";
    private String orderAcceptby = "";
    private String customerId = "";
    private String customerName = "";
    private String customerPhoneNum = "";
    private String driverId = "";
    private String driverName = "";
    private String driverPhoneNum = "";
    private String orderStatusId = "";
    private String orderStatusDesc = "";
    private String parcelSizeId = "";
    private String parcelSizeDesc = "";
    private String totalCostDelivery = "";
    private String paymentStatusId = "";
    private String paymentStatusDesc = "";
    private String paymentMethodId = "";
    private String paymentMethodDesc = "";
    private String deliveryDistance = "";

    public CustomerOrder() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public String getDropoffDateTime() {
        return dropoffDateTime;
    }

    public void setDropoffDateTime(String dropoffDateTime) {
        this.dropoffDateTime = dropoffDateTime;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getDropoffLocation() {
        return dropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        this.dropoffLocation = dropoffLocation;
    }

    public String getOrderAcceptby() {
        return orderAcceptby;
    }

    public void setOrderAcceptby(String orderAcceptby) {
        this.orderAcceptby = orderAcceptby;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(String orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getParcelSizeId() {
        return parcelSizeId;
    }

    public void setParcelSizeId(String parcelSizeId) {
        this.parcelSizeId = parcelSizeId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNum() {
        return customerPhoneNum;
    }

    public void setCustomerPhoneNum(String customerPhoneNum) {
        this.customerPhoneNum = customerPhoneNum;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhoneNum() {
        return driverPhoneNum;
    }

    public void setDriverPhoneNum(String driverPhoneNum) {
        this.driverPhoneNum = driverPhoneNum;
    }

    public String getOrderStatusDesc() {
        return orderStatusDesc;
    }

    public void setOrderStatusDesc(String orderStatusDesc) {
        this.orderStatusDesc = orderStatusDesc;
    }

    public String getParcelSizeDesc() {
        return parcelSizeDesc;
    }

    public void setParcelSizeDesc(String parcelSizeDesc) {
        this.parcelSizeDesc = parcelSizeDesc;
    }

    public String getTotalCostDelivery() {
        return totalCostDelivery;
    }

    public void setTotalCostDelivery(String totalCostDelivery) {
        this.totalCostDelivery = totalCostDelivery;
    }

    public String getPaymentStatusId() {
        return paymentStatusId;
    }

    public void setPaymentStatusId(String paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }

    public String getPaymentStatusDesc() {
        return paymentStatusDesc;
    }

    public void setPaymentStatusDesc(String paymentStatusDesc) {
        this.paymentStatusDesc = paymentStatusDesc;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodDesc() {
        return paymentMethodDesc;
    }

    public void setPaymentMethodDesc(String paymentMethodDesc) {
        this.paymentMethodDesc = paymentMethodDesc;
    }

    public String getDeliveryDistance() {
        return deliveryDistance;
    }

    public void setDeliveryDistance(String deliveryDistance) {
        this.deliveryDistance = deliveryDistance;
    }
}
