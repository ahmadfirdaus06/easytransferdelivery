package com.vienod.easytransferdelivery.models;

public class NewOrderData {

    private String parcelSizeId = "";
    private String pickupDateTime = "";
    private String pickupLocation = "";
    private String dropoffDateTime = "";
    private String dropoffLocation = "";
    private String paymentMethodId = "";
    private String cardNumber = "";
    private String expDate = "";
    private String cvv = "";
    private String orderCost = "";
    private String parcelSizeDesc = "";
    private String distance = "";
    private String paymentStatusId = "";

    public NewOrderData() {
    }

    public String getParcelSizeId() {
        return parcelSizeId;
    }

    public void setParcelSizeId(String parcelSizeId) {
        this.parcelSizeId = parcelSizeId;
    }

    public String getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getDropoffDateTime() {
        return dropoffDateTime;
    }

    public void setDropoffDateTime(String dropoffDateTime) {
        this.dropoffDateTime = dropoffDateTime;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getDropoffLocation() {
        return dropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        this.dropoffLocation = dropoffLocation;
    }

    public String getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(String orderCost) {
        this.orderCost = orderCost;
    }

    public String getParcelSizeDesc() {
        return parcelSizeDesc;
    }

    public void setParcelSizeDesc(String parcelSizeDesc) {
        this.parcelSizeDesc = parcelSizeDesc;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getPaymentStatusId() {
        return paymentStatusId;
    }

    public void setPaymentStatusId(String paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }
}
