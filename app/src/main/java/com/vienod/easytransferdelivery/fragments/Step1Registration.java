package com.vienod.easytransferdelivery.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.models.RegistrationData;

public class Step1Registration extends Fragment {

    private Button buttonSelectCustomer, buttonSelectDriver;
    private Cache cache;
    private RegistrationData registrationData;
    private ConnectionCheck conn;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reqistration_step_1,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        buttonSelectCustomer = view.findViewById(R.id.button_select_customer);
        buttonSelectDriver = view.findViewById(R.id.button_select_driver);
        cache = new Cache(getActivity());
        conn = new ConnectionCheck(getActivity());
        setup();
    }

    public void setup(){
        buttonSelectCustomer.setOnClickListener(next);
        buttonSelectDriver.setOnClickListener(next);
        cache.removeRegistrationData();
    }

    private View.OnClickListener next = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button button = (Button)v;

            registrationData = new RegistrationData();

            if (button.getText().toString().equals("Customer")){
                registrationData.setUserTypeId(2);
            }
            else{
                registrationData.setUserTypeId(3);
            }

            if (cache.setRegistrationData(registrationData) && cache.getRegistrationData() != null && conn.isOnline()){
                Step2Registration step2Registration = new Step2Registration();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                        .replace(R.id.container, step2Registration)
                        .addToBackStack(null).commit();
            }
        }
    };
}
