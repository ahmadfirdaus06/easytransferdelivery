package com.vienod.easytransferdelivery.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.adapters.ReportListAdapter;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.CustomerOrder;
import com.vienod.easytransferdelivery.models.Report;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReportFragment extends Fragment {

    private Toolbar toolbar;
    private TextView textTotalEarned, textTotalRating;
    private RecyclerView listReport;
    private LinearLayout noDetails;
    private SwipeRefreshLayout refresh;
    private ConnectionCheck conn;
    private RequestQueue requestQueue;
    private DataSource dataSource;
    private Cache cache;
    private ArrayList<Report> reportList = new ArrayList<>();
    private ReportListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.report_fragment,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        toolbar = view.findViewById(R.id.toolbar);
        textTotalEarned = view.findViewById(R.id.text_total_earned);
        textTotalRating = view.findViewById(R.id.text_total_rating);
        listReport = view.findViewById(R.id.list_report);
        noDetails = view.findViewById(R.id.no_details);
        refresh = view.findViewById(R.id.refresh);
        setup();
    }

    public void setup(){
        conn = new ConnectionCheck(getActivity());
        dataSource = new DataSource();
        cache = new Cache(getActivity());
        toolbar.setTitle("My Assessment");
        toolbar.setSubtitle("Total Earned and Rating");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        getData(false);
        refresh.setOnRefreshListener(refreshListener);
    }

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            getData(true);
        }
    };


    public void getData(Boolean manual){

        if (conn.isOnline()){
            if (manual){
                refresh.setRefreshing(true);
            }
            JSONObject requestObj = new JSONObject();
            try {
                requestObj.put("driver_id", cache.getUserPref().getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            requestQueue = Volley.newRequestQueue(getActivity());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.POST, dataSource.getReports(), requestObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getBoolean("status")){
                            reportList.clear();
                            JSONArray objectArr = response.getJSONArray("data");
                            double totalAllEarned = 0.00;
                            float totalAllRating = 0;
                            for (int i = 0; i < objectArr.length(); i++){
                                Report report = new Report();
                                report.setId(objectArr.getJSONObject(i).getString("id"));
                                report.setCustomerOrderId(objectArr.getJSONObject(i).getString("customer_order_id"));
                                report.setDriverId(objectArr.getJSONObject(i).getString("driver_id"));
                                report.setTotalEarned(objectArr.getJSONObject(i).getString("total_earned"));
                                report.setTotalRating(objectArr.getJSONObject(i).getString("total_rating"));
                                report.setCreatedAt(objectArr.getJSONObject(i).getString("created_at"));
                                report.setUpdatedAt(objectArr.getJSONObject(i).getString("updated_at"));
                                totalAllEarned = totalAllEarned + Double.parseDouble(report.getTotalEarned());
                                totalAllRating = totalAllRating + Float.parseFloat(report.getTotalRating());
                                reportList.add(report);
                            }
                            textTotalEarned.setText("RM " + totalAllEarned);
                            textTotalRating.setText("(" + totalAllRating + " Ratings)");
                            showList(reportList);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hideList();
                    }
                    refresh.setRefreshing(false);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    refresh.setRefreshing(false);
                    System.out.println(error);
                    hideList();
                }
            }
            );

            requestQueue.add(jsonObjectRequest);
        }
        else{
            refresh.setRefreshing(false);
        }
    }

    public void showList(ArrayList<Report> reportList){

        layoutManager = new LinearLayoutManager(getActivity());
        adapter = new ReportListAdapter(getActivity(), reportList, new ReportListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Report report) {
                OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("order_id", report.getCustomerOrderId());
                orderDetailsFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                        .add(R.id.container, orderDetailsFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        listReport.setAdapter(adapter);
        listReport.setVisibility(View.VISIBLE);
        listReport.setLayoutManager(layoutManager);
        listReport.setItemAnimator(new DefaultItemAnimator());
        listReport.addItemDecoration(new DividerItemDecoration(listReport.getContext(),
                LinearLayoutManager.VERTICAL));
        listReport.setVisibility(View.VISIBLE);
        noDetails.setVisibility(View.GONE);
    }

    public void hideList(){
        listReport.setVisibility(View.GONE);
        noDetails.setVisibility(View.VISIBLE);
    }
}
