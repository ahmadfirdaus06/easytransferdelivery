package com.vienod.easytransferdelivery.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.RegistrationData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Step3Registration extends Fragment {
    private Button finishButton;
    private EditText inputUsername, inputPassword, inputConfirmPassword;
    private ProgressBar progressBar;
    private ConnectionCheck conn;
    private RequestQueue requestQueue;
    private DataSource dataSource;
    private Cache cache;
    private RegistrationData registrationData;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reqistration_step_3,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        finishButton = view.findViewById(R.id.finish_button);
        inputUsername = view.findViewById(R.id.input_username);
        inputPassword = view.findViewById(R.id.input_password);
        inputConfirmPassword = view.findViewById(R.id.input_confirm_password);
        progressBar = view.findViewById(R.id.progress_bar);
        setup();
    }

    public void setup(){
        finishButton.setOnClickListener(finish);
        conn = new ConnectionCheck(getActivity());
        dataSource = new DataSource();
        cache = new Cache(getActivity());
        if (cache.getRegistrationData() != null){
            registrationData = cache.getRegistrationData();
            inputUsername.setText(registrationData.getUsername());
            inputPassword.setText(registrationData.getPassword());
        }
    }

    private View.OnClickListener finish = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final String username = inputUsername.getText().toString().trim();
            final String password = inputPassword.getText().toString().trim();
            String confirmPassword = inputConfirmPassword.getText().toString().trim();

            if (username.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()){

                if (username.isEmpty()){
                    inputUsername.setHint("Username field is required");
                }

                if (password.isEmpty()){
                    inputPassword.setHint("Password field is required");
                }

                if (confirmPassword.isEmpty()){
                    inputConfirmPassword.setHint("Password match field is required");
                }
            }
            else{
                if (confirmPassword.equals(password)){

                    if (conn.isOnline()){
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.setIndeterminate(true);
                        JSONObject requestObj = new JSONObject();
                        try {
                            requestObj.put("username", username);
                            requestObj.put("user_type_id", cache.getRegistrationData().getUserTypeId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        requestQueue = Volley.newRequestQueue(getActivity());
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                                Request.Method.POST, dataSource.getRegistrationCheckUrl(), requestObj, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getBoolean("status")){

                                        if (cache.getRegistrationData() != null){
                                            registrationData = cache.getRegistrationData();
                                            registrationData.setUsername(username);
                                            registrationData.setPassword(password);

                                            if (cache.setRegistrationData(registrationData)){
                                                register();
                                            }
                                        }
                                    }
                                    else{
                                        progressBar.setVisibility(View.INVISIBLE);
                                        inputUsername.setText("");
                                        inputUsername.setHint("Username already taken");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressBar.setVisibility(View.INVISIBLE);
                                System.out.println(error);
                            }
                        }
                        );

                        requestQueue.add(jsonObjectRequest);
                    }

                }
                else{
                    inputConfirmPassword.setText("");
                    inputConfirmPassword.setHint("Password must be same");
                }
            }
        }
    };

    public void register(){
        registrationData = cache.getRegistrationData();
        JSONObject requestObj = new JSONObject();

        try {
            requestObj.put("name", registrationData.getName());
            requestObj.put("nric", registrationData.getNric());
            requestObj.put("phone_num", registrationData.getPhone_num());
            requestObj.put("email", registrationData.getEmail());
            requestObj.put("username", registrationData.getUsername());
            requestObj.put("password", registrationData.getPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(

                Request.Method.POST, getUrlByRole(registrationData.getUserTypeId()), requestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println(response);
                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();

                    if (response.getBoolean("status")){
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();
                            LoginFragment loginFragment = new LoginFragment();
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                                    .replace(R.id.container, loginFragment).commit();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        }
        );

        requestQueue.add(jsonObjectRequest);
    }

    public String getUrlByRole(int roleId){
        if (roleId == 2){
            return dataSource.getCustomerJoinUrl();
        }
        else if (roleId == 3){
            return dataSource.getStaffJoinRequestUrl();
        }
        return null;
    }
}
