package com.vienod.easytransferdelivery.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.RegistrationData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Step2Registration extends Fragment {
    private Button nextButton;
    private EditText inputName, inputNric, inputPhoneNum, inputEmail;
    private DataSource dataSource;
    private Cache cache;
    private ConnectionCheck conn;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;
    private RegistrationData registrationData;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reqistration_step_2,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        nextButton = view.findViewById(R.id.next_button);
        inputName = view.findViewById(R.id.input_name);
        inputNric = view.findViewById(R.id.input_nric);
        inputPhoneNum = view.findViewById(R.id.input_phone_num);
        inputEmail = view.findViewById(R.id.input_email);
        progressBar = view.findViewById(R.id.progress_bar);
        setup();
    }

    public void setup(){
        nextButton.setOnClickListener(next);
        dataSource = new DataSource();
        cache = new Cache(getActivity());
        conn = new ConnectionCheck(getActivity());
        if (cache.getRegistrationData() != null){
            registrationData = cache.getRegistrationData();
            inputName.setText(registrationData.getName());
            inputNric.setText(registrationData.getNric());
            inputPhoneNum.setText(registrationData.getPhone_num());
            inputEmail.setText(registrationData.getEmail());
        }
    }

    private View.OnClickListener next = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final String name = inputName.getText().toString().trim();
            final String nric = inputNric.getText().toString().trim();
            final String phoneNum = inputPhoneNum.getText().toString().trim();
            final String email = inputEmail.getText().toString().trim();

            if (name.isEmpty() || nric.isEmpty() || phoneNum.isEmpty() || email.isEmpty()){

                if (name.isEmpty()){
                    inputName.setHint("Name field is required");
                }

                if (nric.isEmpty()){
                    inputNric.setHint("NRIC field is required");
                }

                if (phoneNum.isEmpty()){
                    inputPhoneNum.setHint("Contact number field is required");
                }

                if (email.isEmpty()){
                    inputEmail.setHint("Email field is required");
                }

            }
            else{
                if (conn.isOnline()){
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);
                    JSONObject requestObj = new JSONObject();
                    try {
                        requestObj.put("email", email);
                        requestObj.put("user_type_id", cache.getRegistrationData().getUserTypeId());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    requestQueue = Volley.newRequestQueue(getActivity());
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                            Request.Method.POST, dataSource.getRegistrationCheckUrl(), requestObj, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("status")){

                                    if (cache.getRegistrationData() != null){
                                        registrationData = cache.getRegistrationData();
                                        registrationData.setName(name);
                                        registrationData.setNric(nric);
                                        registrationData.setPhone_num(phoneNum);
                                        registrationData.setEmail(email);

                                        if (cache.setRegistrationData(registrationData)){
                                            Step3Registration step3Registration = new Step3Registration();
                                            getActivity().getSupportFragmentManager().
                                                    beginTransaction()
                                                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                                                    .replace(R.id.container, step3Registration)
                                                    .addToBackStack(null).commit();
                                        }
                                    }

                                }
                                else{
                                    progressBar.setVisibility(View.INVISIBLE);
                                    inputEmail.setText("");
                                    inputEmail.setHint("Email already registered");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressBar.setVisibility(View.INVISIBLE);
                            System.out.println(error);
                        }
                    }
                    );

                    requestQueue.add(jsonObjectRequest);
                }
            }

        }
    };
}
