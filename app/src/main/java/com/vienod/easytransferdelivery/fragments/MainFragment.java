package com.vienod.easytransferdelivery.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.UserPref;

import org.json.JSONException;
import org.json.JSONObject;

public class MainFragment extends Fragment {

    private BottomNavigationView bottomNav;
    private RequestQueue requestQueue;
    private Cache cache;
    private DataSource dataSource;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        bottomNav = view.findViewById(R.id.bottom_nav);
        setup();
    }

    public void setup(){
        bottomNav.setOnNavigationItemSelectedListener(bottomNavListener);
        bottomNav.setSelectedItemId(R.id.home);
        bottomNav.performClick();
        cache = new Cache(getActivity());
        dataSource = new DataSource();
        if (cache.getUserPref().getUserTypeId().equals("2")){
            bottomNav.getMenu().removeItem(R.id.report);
        }
        updateUserPref();
    }

    public void updateUserPref(){
        JSONObject requestObj = new JSONObject();
        try {
            requestObj.put("id", cache.getUserPref().getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, dataSource.getUserData(), requestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    if (response.getBoolean("status")){

                        UserPref userPref = new UserPref();
                        JSONObject responseObj = response.getJSONObject("data");
                        userPref.setId(responseObj.getString("id"));
                        userPref.setUsername(responseObj.getString("username"));
                        userPref.setName(responseObj.getString("name"));
                        userPref.setPassword(responseObj.getString("password"));
                        userPref.setNric(responseObj.getString("nric"));
                        userPref.setCreatedAt(responseObj.getString("created_at"));
                        userPref.setUpdatedAt(responseObj.getString("updated_at"));
                        userPref.setPhoneNum(responseObj.getString("phone_num"));
                        userPref.setUserStatusId(responseObj.getString("user_status_id"));
                        userPref.setUserTypeId(responseObj.getString("user_type_id"));
                        cache.setUserPref(userPref);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        }
        );

        requestQueue.add(jsonObjectRequest);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch(item.getItemId()){
                case R.id.home:
                    HomeFragment homeFragment = new HomeFragment();
                    getActivity().getSupportFragmentManager().
                            beginTransaction()
                            .replace(R.id.container_main_fragment, homeFragment)
                            .commit();
                    return true;

                case R.id.order:
                    OrderFragment orderFragment = new OrderFragment();
                    getActivity().getSupportFragmentManager().
                            beginTransaction()
                            .replace(R.id.container_main_fragment, orderFragment)
                            .commit();
                    return true;

                case R.id.report:
                    System.out.println("Report");
                    ReportFragment reportFragment = new ReportFragment();
                    getActivity().getSupportFragmentManager().
                            beginTransaction()
                            .replace(R.id.container_main_fragment, reportFragment)
                            .commit();
                    return true;

                case R.id.profile:
                    ProfileFragment profileFragment = new ProfileFragment();
                    getActivity().getSupportFragmentManager().
                            beginTransaction()
                            .replace(R.id.container_main_fragment, profileFragment)
                            .commit();
                    return true;
            }
            return false;
        }
    };
}
