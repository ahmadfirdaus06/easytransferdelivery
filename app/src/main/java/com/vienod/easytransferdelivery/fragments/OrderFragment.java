package com.vienod.easytransferdelivery.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.adapters.OrderListAdapter;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.CustomerOrder;
import com.vienod.easytransferdelivery.models.UserPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrderFragment extends Fragment {

    private FloatingActionButton buttonAddOrder;
    private Cache cache;
    private SwipeRefreshLayout refresh;
    private ConnectionCheck conn;
    private RequestQueue requestQueue;
    private DataSource dataSource;
    private RecyclerView listOrder;
    private OrderListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Toolbar toolbar;
    private LinearLayout noOrder;
    private ArrayList<CustomerOrder> orderList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.order_fragment,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        listOrder = view.findViewById(R.id.list_order);
        buttonAddOrder = view.findViewById(R.id.button_add_order);
        refresh = view.findViewById(R.id.refresh);
        toolbar = view.findViewById(R.id.toolbar);
        noOrder = view.findViewById(R.id.no_order);
        setup();
    }

    public void setup(){
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        UserPref userPref;
        conn = new ConnectionCheck(getActivity());
        dataSource = new DataSource();
        cache = new Cache(getActivity());
        userPref = cache.getUserPref();

        if (userPref.getUserTypeId().equals("2")){
            setupCustomerUI();
        }
        else if (userPref.getUserTypeId().equals("3")){
            setupDriverUI();
        }

        layoutManager = new LinearLayoutManager(getActivity());
        refresh.setOnRefreshListener(refreshListener);

        loadData(false);

    }

    public void setupCustomerUI(){

        buttonAddOrder.setVisibility(View.VISIBLE);
        buttonAddOrder.setOnClickListener(addOrder);
        toolbar.setTitle("My Order");
        toolbar.setSubtitle("");
    }

    public void setupDriverUI(){
        buttonAddOrder.setVisibility(View.INVISIBLE);
        toolbar.setTitle("Customer Order");
        toolbar.setSubtitle("Available order request");
    }

    private View.OnClickListener addOrder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Step1AddOrderFragment step1AddOrderFragment = new Step1AddOrderFragment();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                    .add(R.id.container, step1AddOrderFragment)
                    .addToBackStack(null)
                    .commit();
        }
    };

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            loadData(true);
        }
    };

    public void showUi(){
        listOrder.setVisibility(View.VISIBLE);
        noOrder.setVisibility(View.GONE);
    }


    public void hideUi(){
        noOrder.setVisibility(View.VISIBLE);
        listOrder.setVisibility(View.GONE);
    }

    public void loadData(Boolean manual){

        if (conn.isOnline()){
            if (manual){
                refresh.setRefreshing(true);
            }
            JSONObject requestObj = new JSONObject();
            try {
                requestObj.put("user_type_id", cache.getUserPref().getUserTypeId());
                requestObj.put("customer_id", cache.getUserPref().getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            requestQueue = Volley.newRequestQueue(getActivity());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.POST, dataSource.getOrdersUrl(), requestObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        if (response.getBoolean("status")){
                            JSONArray objectArr = response.getJSONArray("data");
                            orderList.clear();
                            if (objectArr.length() != 0){
                                for (int i = 0; i < objectArr.length(); i++){
                                    CustomerOrder order = new CustomerOrder();
                                    order.setId(objectArr.getJSONObject(i).getString("id"));
                                    order.setCreatedAt(objectArr.getJSONObject(i).getString("created_at"));
                                    order.setOrderStatusId(objectArr.getJSONObject(i).getString("order_status_id"));
                                    order.setDriverId(objectArr.getJSONObject(i).getString("driver_id"));
                                    orderList.add(order);
                                }

                                adapter = new OrderListAdapter(getActivity(), orderList, 1, new OrderListAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(CustomerOrder order) {
                                        OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("order_id", order.getId());
                                        orderDetailsFragment.setArguments(bundle);
                                        getActivity().getSupportFragmentManager()
                                                .beginTransaction()
                                                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                                                .add(R.id.container, orderDetailsFragment)
                                                .addToBackStack(null)
                                                .commit();
                                    }
                                });

                                listOrder.setAdapter(adapter);
                                showUi();
                                listOrder.setLayoutManager(layoutManager);
                                listOrder.setItemAnimator(new DefaultItemAnimator());
                                listOrder.addItemDecoration(new DividerItemDecoration(listOrder.getContext(),
                                        LinearLayoutManager.VERTICAL));
                            }
                            else{
                                hideUi();
                            }
                        }

                        refresh.setRefreshing(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        hideUi();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    refresh.setRefreshing(false);
                    System.out.println(error);
                    hideUi();
                }
            }
            );

            requestQueue.add(jsonObjectRequest);
        }else{
            refresh.setRefreshing(false);
        }

    }
}
