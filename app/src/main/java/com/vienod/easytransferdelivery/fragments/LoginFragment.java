package com.vienod.easytransferdelivery.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.UserPref;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginFragment extends Fragment {

    private Button buttonLogin;
    private TextView textRegister;
    private Cache cache;
    private ProgressBar progressBar;
    private EditText inputUsername, inputPassword;
    private ConnectionCheck conn;
    private RequestQueue requestQueue;
    private DataSource dataSource;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        buttonLogin = view.findViewById(R.id.button_login);
        textRegister = view.findViewById(R.id.text_register);
        progressBar = view.findViewById(R.id.progress_bar);
        inputUsername = view.findViewById(R.id.input_username);
        inputPassword = view.findViewById(R.id.input_password);
        setup();
    }

    private View.OnClickListener login = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String username = inputUsername.getText().toString().trim();
            String password = inputPassword.getText().toString().trim();

            if (username.isEmpty() || password.isEmpty()){
                Toast.makeText(getActivity(), "Please enter your username and password",
                        Toast.LENGTH_SHORT).show();
            }
            else{

                if (conn.isOnline()){
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);
                    JSONObject requestObj = new JSONObject();
                    try {
                        requestObj.put("username", username);
                        requestObj.put("password", password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    requestQueue = Volley.newRequestQueue(getActivity());
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                            Request.Method.POST, dataSource.getUserLoginUrl(), requestObj, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Toast.makeText(getActivity(), response.getString("message"),
                                        Toast.LENGTH_SHORT).show();

                                if (response.getBoolean("status")){

                                    UserPref userPref = new UserPref();
                                    JSONObject responseObj = response.getJSONObject("data");
                                    userPref.setId(responseObj.getString("id"));
                                    userPref.setUsername(responseObj.getString("username"));
                                    userPref.setName(responseObj.getString("name"));
                                    userPref.setPassword(responseObj.getString("password"));
                                    userPref.setNric(responseObj.getString("nric"));
                                    userPref.setCreatedAt(responseObj.getString("created_at"));
                                    userPref.setUpdatedAt(responseObj.getString("updated_at"));
                                    userPref.setPhoneNum(responseObj.getString("phone_num"));
                                    userPref.setUserStatusId(responseObj.getString("user_status_id"));
                                    userPref.setUserTypeId(responseObj.getString("user_type_id"));
                                    if (cache.setUserPref(userPref)){
                                        checkUserPref();
                                    }
                                }
                                else{
                                    progressBar.setVisibility(View.INVISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressBar.setVisibility(View.INVISIBLE);
                            System.out.println(error);
                        }
                    }
                    );

                    requestQueue.add(jsonObjectRequest);
                }

            }

        }
    };

    private View.OnClickListener register = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Step1Registration step1Registration = new Step1Registration();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                    .replace(R.id.container, step1Registration, "main")
                    .addToBackStack(null).commit();
        }
    };

    public void setup(){
        buttonLogin.setOnClickListener(login);
        textRegister.setOnClickListener(register);
        conn = new ConnectionCheck(getActivity());
        dataSource = new DataSource();
        cache = new Cache(getActivity());
        checkUserPref();
    }

    public void checkUserPref(){
        if (cache.getUserPref() != null){
            redirect();
        }
    }

    public void redirect(){
        MainFragment mainFragment = new MainFragment();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(R.id.container, mainFragment)
                .commit();
    }
}
