package com.vienod.easytransferdelivery.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.NewOrderData;
import com.vienod.easytransferdelivery.models.UserPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

public class Step1AddOrderFragment extends Fragment {
    private Cache cache;
    private Button buttonNext;
    private EditText inputPickupLocation, inputDropoffLocation;
    private NewOrderData newOrderData;
    private Spinner parcelSize;
    private TimePicker pickupTimePicker, dropoffTimePicker;
    private DatePicker pickupDatePicker, dropoffDatePicker;
    private ConnectionCheck conn;
    private ProgressBar progressBar;
    private RequestQueue requestQueue;
    private DataSource dataSource;
    private DecimalFormat df = new DecimalFormat("#.00");

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_order_fragment_step_1,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        buttonNext = view.findViewById(R.id.button_next);
        inputPickupLocation = view.findViewById(R.id.input_pickup_location);
        inputDropoffLocation = view.findViewById(R.id.input_dropoff_location);
        parcelSize = view.findViewById(R.id.spinner_parcel_size);
        pickupTimePicker = view.findViewById(R.id.timepicker_pickup);
        dropoffTimePicker = view.findViewById(R.id.timepicker_dropoff);
        pickupDatePicker = view.findViewById(R.id.datepicker_pickup);
        dropoffDatePicker = view.findViewById(R.id.datepicker_dropoff);
        progressBar = view.findViewById(R.id.progress_bar);
        setup();
    }

    public void setup(){
        cache = new Cache(getActivity());
        dataSource = new DataSource();
        conn = new ConnectionCheck(getActivity());
        cache.removeNewOrderData();
        validateAllFields();
        buttonNext.setOnClickListener(next);
        inputPickupLocation.addTextChangedListener(validator);
        inputDropoffLocation.addTextChangedListener(validator);
    }

    private View.OnClickListener next = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final String pickupLocation = inputPickupLocation.getText().toString().trim();
            final String dropoffLocation = inputDropoffLocation.getText().toString().trim();
            String pickupTime = convertTo2Digits(pickupTimePicker.getHour()) + ":" + convertTo2Digits(pickupTimePicker.getMinute()) + ":00";
            String dropoffTime = convertTo2Digits(dropoffTimePicker.getHour()) + ":" + convertTo2Digits(dropoffTimePicker.getMinute()) + ":00";
            String pickupDate = pickupDatePicker.getYear() + "-" + convertTo2Digits(pickupDatePicker.getMonth() + 1) + "-" + convertTo2Digits(pickupDatePicker.getDayOfMonth());
            String dropoffDate = dropoffDatePicker.getYear() + "-" + convertTo2Digits(dropoffDatePicker.getMonth() + 1) + "-" + convertTo2Digits(dropoffDatePicker.getDayOfMonth());
            final String pickupDateTime = pickupDate + " " + pickupTime;
            final String dropoffDateTime = dropoffDate + " " + dropoffTime;
            final int parcelSizeId = parcelSize.getSelectedItemPosition();

            if (conn.isOnline()){
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setIndeterminate(true);
                JSONObject requestObj = new JSONObject();
                try {
                    requestObj.put("id", String.valueOf(parcelSizeId + 1));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                requestQueue = Volley.newRequestQueue(getActivity());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.POST, dataSource.getParcelSizeUrl(), requestObj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (response.getBoolean("status")){

                                double ratePerParcelSize = Double.parseDouble(response.getJSONObject("data").getString("rate"));

                                double ratePerDistance = 5.00;

                                Random random = new Random();
                                double randomDistance = 5 + (50 - 5) * random.nextDouble();

                                double orderCost = randomDistance * ratePerDistance + ratePerParcelSize;

                                newOrderData = new NewOrderData();
                                newOrderData.setParcelSizeDesc(parcelSize.getSelectedItem().toString());
                                newOrderData.setOrderCost(df.format(orderCost));
                                newOrderData.setPickupLocation(pickupLocation);
                                newOrderData.setDropoffLocation(dropoffLocation);
                                newOrderData.setPickupDateTime(pickupDateTime);
                                newOrderData.setDropoffDateTime(dropoffDateTime);
                                newOrderData.setParcelSizeId(String.valueOf(parcelSizeId + 1));
                                newOrderData.setDistance(df.format(randomDistance));

                                if (cache.setNewOrderData(newOrderData)){

                                    Step2AddOrderFragment step2AddOrderFragment = new Step2AddOrderFragment();
                                    getActivity().getSupportFragmentManager()
                                            .beginTransaction()
                                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                                            .add(R.id.container, step2AddOrderFragment)
                                            .addToBackStack(null)
                                            .commit();
                                }
                            }
                            else{
                                Toast.makeText(getActivity(), "Failed to retrieve data", Toast.LENGTH_SHORT).show();
                            }
                            progressBar.setVisibility(View.INVISIBLE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.INVISIBLE);
                        System.out.println(error);
                    }
                }
                );

                requestQueue.add(jsonObjectRequest);
            }
        }
    };

    public String convertTo2Digits(int num){
        if (num < 10){
            return "0" + num;
        }
        return String.valueOf(num);
    }

    private TextWatcher validator = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            validateAllFields();
        }
    };

    public void validateAllFields(){
        String pickupLocation = inputPickupLocation.getText().toString().trim();
        String dropoffLocation = inputDropoffLocation.getText().toString().trim();

        if (pickupLocation.isEmpty() || dropoffLocation.isEmpty()){
            buttonNext.setVisibility(View.GONE);
        }
        else{
            buttonNext.setVisibility(View.VISIBLE);
        }
    }
}
