package com.vienod.easytransferdelivery.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;

public class ProfileFragment extends Fragment {

    private Button buttonLogout;
    private Cache cache;
    private Toolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        buttonLogout = view.findViewById(R.id.button_logout);
        toolbar = view.findViewById(R.id.toolbar);
        setup();
    }

    public void setup(){
        buttonLogout.setOnClickListener(logout);
        cache = new Cache(getActivity());
        toolbar.setTitle("My Profile");
        toolbar.setTitleTextColor(Color.WHITE);
    }

    private View.OnClickListener logout = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (cache.removeUserPref()){
                LoginFragment loginFragment = new LoginFragment();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                        .replace(R.id.container, loginFragment)
                        .commit();
                Toast.makeText(getActivity(),"Logout Successful", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getActivity(),"Logout Failed", Toast.LENGTH_SHORT).show();
            }
        }
    };
}
