package com.vienod.easytransferdelivery.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.CustomerOrder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrderDetailsFragment extends Fragment {

    private Toolbar toolbar;
    private SwipeRefreshLayout refresh;
    private Bundle data;
    private ConnectionCheck conn;
    private RequestQueue requestQueue;
    private DataSource dataSource;
    private TextView textOrderStatus, textParcelSizeDesc, textPickupLocation, textDropoffLocation,
            textDeliveryDistance, textPickupDateTime, textDropoffDateTime, textCreatedAt,
            textTotalDeliveryCost, textPaymentMethodDesc, textPaymentStatusDesc, textCustomerName,
            textCustomerPhoneNum, textDriverName, textDriverPhoneNum, textOrderAccepted;
    private ScrollView mainUi;
    private LinearLayout noOrder, ratingLayout;
    private RatingBar rating;
    private Cache cache;
    private Button buttonApprove;
    private CustomerOrder order;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.order_details_fragment,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        toolbar = view.findViewById(R.id.toolbar);
        refresh = view.findViewById(R.id.refresh);
        textOrderStatus = view.findViewById(R.id.text_order_status_desc);
        textParcelSizeDesc = view.findViewById(R.id.text_parcel_size_desc);
        textPickupLocation = view.findViewById(R.id.text_pickup_location);
        textDropoffLocation = view.findViewById(R.id.text_dropoff_location);
        textDeliveryDistance = view.findViewById(R.id.text_distance);
        textPickupDateTime = view.findViewById(R.id.text_pickup_date_time);
        textDropoffDateTime = view.findViewById(R.id.text_dropoff_date_time);
        textCreatedAt = view.findViewById(R.id.text_created_at);
        textTotalDeliveryCost = view.findViewById(R.id.text_total_delivery_cost);
        textPaymentMethodDesc = view.findViewById(R.id.text_payment_method_desc);
        textPaymentStatusDesc = view.findViewById(R.id.text_payment_status_desc);
        textCustomerName = view.findViewById(R.id.text_customer_name);
        textCustomerPhoneNum = view.findViewById(R.id.text_customer_phone_num);
        textDriverName = view.findViewById(R.id.text_driver_name);
        textDriverPhoneNum = view.findViewById(R.id.text_driver_phone_num);
        textOrderAccepted = view.findViewById(R.id.text_accept_by);
        mainUi = view.findViewById(R.id.main_ui);
        noOrder = view.findViewById(R.id.no_order_details);
        buttonApprove = view.findViewById(R.id.button_accept_order_request);
        rating = view.findViewById(R.id.rating);
        ratingLayout = view.findViewById(R.id.rating_layout);
        setup();
    }

    public void setup(){
        data = getArguments();
        conn = new ConnectionCheck(getActivity());
        dataSource = new DataSource();
        cache = new Cache(getActivity());
        refresh.setOnRefreshListener(refreshListener);
        mainUi.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (mainUi.getScrollY() == 0){
                    refresh.setEnabled(true);
                }
                else{
                    refresh.setEnabled(false);
                }
            }
        });

        if (data != null){
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setSubtitleTextColor(Color.WHITE);
            toolbar.setTitle("Order #" + data.getString("order_id"));
            toolbar.setSubtitle("Details");
            loadData(false, data.getString("order_id"));
        }
    }

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {

            loadData(true, data.getString("order_id"));
        }
    };

    public void loadData(Boolean manual, String orderId){
        if (conn.isOnline()){
            if (manual){
                refresh.setRefreshing(true);
            }
            JSONObject requestObj = new JSONObject();
            try {
                requestObj.put("order_id", orderId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            requestQueue = Volley.newRequestQueue(getActivity());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.POST, dataSource.getOrdersUrl(), requestObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {

                        if (response.getBoolean("status")){
                            JSONArray objectArr = response.getJSONArray("data");
                            if (objectArr.length() != 0){
                                JSONObject responseObj = objectArr.getJSONObject(0);
                                order = new CustomerOrder();
                                order.setId(responseObj.getString("id"));
                                order.setCreatedAt(responseObj.getString("created_at"));
                                order.setCustomerName(responseObj.getString("customer_name"));
                                order.setCustomerPhoneNum(responseObj.getString("customer_phone_num"));
                                order.setDriverName(responseObj.getString("driver_name"));
                                order.setDriverPhoneNum(responseObj.getString("driver_phone_num"));
                                order.setDropoffDateTime(responseObj.getString("dropoff_date_time"));
                                order.setDropoffLocation(responseObj.getString("dropoff_location"));
                                order.setPickupDateTime(responseObj.getString("pickup_date_time"));
                                order.setPickupLocation(responseObj.getString("pickup_location"));
                                order.setOrderAcceptby(responseObj.getString("order_accept_by"));
                                order.setParcelSizeDesc(responseObj.getString("parcel_size_desc"));
                                order.setOrderStatusDesc(responseObj.getString("order_status_desc"));
                                order.setTotalCostDelivery(responseObj.getString("total_cost_delivery"));
                                order.setPaymentStatusDesc(responseObj.getString("payment_status_desc"));
                                order.setPaymentMethodDesc(responseObj.getString("payment_method_desc"));
                                order.setDeliveryDistance(responseObj.getString("delivery_distance"));
                                order.setDriverId(responseObj.getString("driver_id"));
                                order.setOrderStatusId(responseObj.getString("order_status_id"));
                                showUI(order);
                            }
                            else{
                                hideUI();
                            }
                        }

                        refresh.setRefreshing(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    refresh.setRefreshing(false);
                    System.out.println(error);
                }
            }
            );

            requestQueue.add(jsonObjectRequest);
        }
        else{
            refresh.setRefreshing(false);
            hideUI();
            buttonApprove.setVisibility(View.GONE);
        }

    }

    public void showUI(CustomerOrder order){
        textOrderStatus.setText(order.getOrderStatusDesc());
        textParcelSizeDesc.setText(order.getParcelSizeDesc());
        textPickupLocation.setText(order.getPickupLocation());
        textDropoffLocation.setText(order.getDropoffLocation());
        textDeliveryDistance.setText(order.getDeliveryDistance() + " KM");
        textPickupDateTime.setText(order.getPickupDateTime());
        textDropoffDateTime.setText(order.getDropoffDateTime());
        textCreatedAt.setText(order.getCreatedAt());
        textTotalDeliveryCost.setText("RM " + order.getTotalCostDelivery());
        textPaymentMethodDesc.setText(order.getPaymentMethodDesc());
        textPaymentStatusDesc.setText(order.getPaymentStatusDesc());
        textCustomerName.setText(order.getCustomerName());
        textCustomerPhoneNum.setText(order.getCustomerPhoneNum());
        textOrderAccepted.setText(order.getOrderAcceptby());

        if (order.getDriverId().equals("null")){
            textDriverName.setText("TBD");
            textDriverPhoneNum.setText("TBD");
            textOrderAccepted.setText("TBD");

        }
        else{
            textDriverName.setText(order.getDriverName());
            textDriverPhoneNum.setText(order.getDriverPhoneNum());
            textOrderAccepted.setText(order.getOrderAcceptby());
        }

        if (order.getOrderStatusId().equals("1")){
            if (order.getDriverId().equals("null")){
                buttonApprove.setText("Accept Request");
                buttonApprove.setOnClickListener(accept);
            }
            else{
                buttonApprove.setText("Pickup Order");
                buttonApprove.setOnClickListener(pickup);
            }
        }

        if (order.getOrderStatusId().equals("2")){
            buttonApprove.setText("Dropoff Order");
            buttonApprove.setOnClickListener(dropoff);
        }

        mainUi.setVisibility(View.VISIBLE);
        noOrder.setVisibility(View.GONE);

        if (cache.getUserPref().getUserTypeId().equals("2")){
            if (order.getOrderStatusId().equals("3")){
                ratingChange();
                buttonApprove.setVisibility(View.VISIBLE);
                ratingLayout.setVisibility(View.VISIBLE);
                rating.setOnRatingBarChangeListener(ratingListener);
                buttonApprove.setText("Rate Us");
                buttonApprove.setOnClickListener(rate);
            }
            else{
                ratingChange();
                buttonApprove.setVisibility(View.GONE);
                ratingLayout.setVisibility(View.GONE);
            }
        }

        if (cache.getUserPref().getUserTypeId().equals("3") && !order.getOrderStatusId().equals("3")){
            buttonApprove.setVisibility(View.VISIBLE);
        }

        if (order.getOrderStatusId().equals("4")){
            buttonApprove.setVisibility(View.GONE);
        }
    }

    public void hideUI(){
        mainUi.setVisibility(View.GONE);
        noOrder.setVisibility(View.VISIBLE);
    }

    private View.OnClickListener accept = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (conn.isOnline()){
                buttonApprove.setText("Please wait...");
                JSONObject requestObj = new JSONObject();
                try {
                    requestObj.put("update_flag", "ACCEPT");
                    requestObj.put("driver_id", cache.getUserPref().getId());
                    requestObj.put("order_id", data.getString("order_id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                requestQueue = Volley.newRequestQueue(getActivity());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.POST, dataSource.getOrderUpdateUrl(), requestObj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();

                            if (response.getBoolean("status")){
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                                    fm.popBackStack();
                                    MainFragment mainFragment = new MainFragment();
                                    getActivity().getSupportFragmentManager()
                                            .beginTransaction()
//                                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                                            .replace(R.id.container, mainFragment).commit();
                                }
                            }
                            else{
                                buttonApprove.setText("Accept Request");
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            buttonApprove.setText("Accept Request");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        refresh.setRefreshing(false);
                        System.out.println(error);
                        buttonApprove.setText("Accept Request");
                    }
                }
                );

                requestQueue.add(jsonObjectRequest);
            }

        }
    };

    private RatingBar.OnRatingBarChangeListener ratingListener = new RatingBar.OnRatingBarChangeListener() {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
            ratingChange();
        }
    };

    public void ratingChange(){
        if (rating.getRating() == 0.0){
            buttonApprove.setClickable(false);
            buttonApprove.setEnabled(false);
            buttonApprove.setFocusable(false);
            buttonApprove.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        }

        if (rating.getRating() > 0.0){
            buttonApprove.setClickable(true);
            buttonApprove.setEnabled(true);
            buttonApprove.setFocusable(true);
            buttonApprove.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        }
    }

    private View.OnClickListener pickup = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (conn.isOnline()){
                buttonApprove.setText("Please wait...");
                JSONObject requestObj = new JSONObject();
                try {
                    requestObj.put("update_flag", "PICKUP");
                    requestObj.put("order_id", data.getString("order_id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                requestQueue = Volley.newRequestQueue(getActivity());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.POST, dataSource.getOrderUpdateUrl(), requestObj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();

                            if (response.getBoolean("status")){
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                                    fm.popBackStack();
                                    MainFragment mainFragment = new MainFragment();
                                    getActivity().getSupportFragmentManager()
                                            .beginTransaction()
//                                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                                            .replace(R.id.container, mainFragment).commit();
                                }
                            }
                            else{
                                buttonApprove.setText("Pickup Order");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            buttonApprove.setText("Pickup Order");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        refresh.setRefreshing(false);
                        System.out.println(error);
                        buttonApprove.setText("Pickup Order");
                    }
                }
                );

                requestQueue.add(jsonObjectRequest);
            }
        }
    };

    private View.OnClickListener dropoff = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (conn.isOnline()){
                buttonApprove.setText("Please wait...");
                JSONObject requestObj = new JSONObject();
                try {
                    requestObj.put("update_flag", "DELIVERED");
                    requestObj.put("order_id", data.getString("order_id"));
                    requestObj.put("total_earned", order.getTotalCostDelivery());
                    requestObj.put("driver_id", order.getDriverId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                requestQueue = Volley.newRequestQueue(getActivity());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.POST, dataSource.getOrderUpdateUrl(), requestObj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();

                            if (response.getBoolean("status")){
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                                    fm.popBackStack();
                                    MainFragment mainFragment = new MainFragment();
                                    getActivity().getSupportFragmentManager()
                                            .beginTransaction()
//                                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                                            .replace(R.id.container, mainFragment).commit();
                                }
                            }
                            else{
                                buttonApprove.setText("Dropoff Order");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            buttonApprove.setText("Dropoff Order");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        refresh.setRefreshing(false);
                        System.out.println(error);
                        buttonApprove.setText("Dropoff Order");
                    }
                }
                );

                requestQueue.add(jsonObjectRequest);
            }
        }
    };

    private View.OnClickListener rate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (conn.isOnline()){
                buttonApprove.setText("Please wait...");
                JSONObject requestObj = new JSONObject();
                try {
                    requestObj.put("update_flag", "CLOSE");
                    requestObj.put("order_id", data.getString("order_id"));
                    requestObj.put("total_rating", rating.getRating());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                requestQueue = Volley.newRequestQueue(getActivity());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.POST, dataSource.getOrderUpdateUrl(), requestObj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();

                            if (response.getBoolean("status")){
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                                    fm.popBackStack();
                                    MainFragment mainFragment = new MainFragment();
                                    getActivity().getSupportFragmentManager()
                                            .beginTransaction()
                                            .replace(R.id.container, mainFragment).commit();
                                }
                            }
                            else{
                                buttonApprove.setText("Rate Us");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            buttonApprove.setText("Rate Us");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        refresh.setRefreshing(false);
                        System.out.println(error);
                        buttonApprove.setText("Rate Us");
                    }
                }
                );

                requestQueue.add(jsonObjectRequest);
            }
        }
    };


}
