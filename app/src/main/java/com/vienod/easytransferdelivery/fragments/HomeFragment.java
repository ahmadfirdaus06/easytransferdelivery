package com.vienod.easytransferdelivery.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.adapters.OrderListAdapter;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.CustomerOrder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private Toolbar toolbar;
    private Cache cache;
    private SwipeRefreshLayout refresh;
    private ConnectionCheck conn;
    private RequestQueue requestQueue;
    private DataSource dataSource;
    private CardView driverLayout;
    private RecyclerView customerLayout;
    private TextView textOrderStatusDesc, textCustomerName, textCustomerPhoneNum, textDriverName,
            textDriverPhoneNum, textCreatedOn, textOrderAcceptBy, textOrderId;
    private LinearLayout viewDetails, noRecentOrder;
    private OrderListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<CustomerOrder> orderList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        toolbar = view.findViewById(R.id.toolbar);
        refresh = view.findViewById(R.id.refresh);
        driverLayout = view.findViewById(R.id.driver_layout);
        customerLayout = view.findViewById(R.id.customer_layout);
        textOrderStatusDesc = view.findViewById(R.id.text_order_status_desc);
        textCustomerName = view.findViewById(R.id.text_customer_name);
        textCustomerPhoneNum = view.findViewById(R.id.text_customer_phone_num);
        textDriverName = view.findViewById(R.id.text_driver_name);
        textDriverPhoneNum = view.findViewById(R.id.text_driver_phone_num);
        textOrderId = view.findViewById(R.id.text_order_id);
        textCreatedOn = view.findViewById(R.id.text_created_at);
        textOrderAcceptBy = view.findViewById(R.id.text_accept_by);
        viewDetails = view.findViewById(R.id.info);
        noRecentOrder = view.findViewById(R.id.no_recent_order);
        setup();
    }

    public void setup(){
        cache = new Cache(getActivity());
        conn = new ConnectionCheck(getActivity());
        dataSource = new DataSource();
        toolbar.setTitle("Welcome, " + cache.getUserPref().getName());
        toolbar.setSubtitleTextColor(Color.WHITE);
        toolbar.setTitleTextColor(Color.WHITE);
        if (cache.getUserPref().getUserTypeId().equals("2")){
            getCustomerData(false);
            toolbar.setSubtitle("Your recent order");
        }
        else if (cache.getUserPref().getUserTypeId().equals("3")){
            getDriverData(false);
            toolbar.setSubtitle("Recent customer order");
        }
        refresh.setOnRefreshListener(refreshListener);
    }

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (cache.getUserPref().getUserTypeId().equals("2")){
                getCustomerData(true);
            }
            else if (cache.getUserPref().getUserTypeId().equals("3")){
                getDriverData(true);
            }
        }
    };

    public void setupCustomerUI(ArrayList<CustomerOrder> orderList){
        layoutManager = new LinearLayoutManager(getActivity());

        adapter = new OrderListAdapter(getActivity(), orderList, 2, new OrderListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(CustomerOrder order) {
                OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("order_id", order.getId());
                orderDetailsFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                        .add(R.id.container, orderDetailsFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        customerLayout.setAdapter(adapter);
        customerLayout.setVisibility(View.VISIBLE);
        customerLayout.setLayoutManager(layoutManager);
        customerLayout.setItemAnimator(new DefaultItemAnimator());
        customerLayout.addItemDecoration(new DividerItemDecoration(customerLayout.getContext(),
                LinearLayoutManager.VERTICAL));
    }

    public void setupDriverUI(final CustomerOrder order){
        textOrderId.setText("Order #" + order.getId());

        if (order.getOrderStatusId().equals("1")){
            textOrderStatusDesc.setText("Need Pickup");
        }

        if (order.getOrderStatusId().equals("2")){
            textOrderStatusDesc.setText("Awaiting Delivery");
        }

        textCustomerName.setText(order.getCustomerName());
        textCustomerPhoneNum.setText(order.getCustomerPhoneNum());
        textDriverName.setText(order.getDriverName());
        textDriverPhoneNum.setText(order.getDriverPhoneNum());
        textCreatedOn.setText(order.getCreatedAt());
        textOrderAcceptBy.setText(order.getOrderAcceptby());
        viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("order_id", order.getId());
                orderDetailsFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                        .add(R.id.container, orderDetailsFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        driverLayout.setVisibility(View.VISIBLE);
    }

    public void getCustomerData(Boolean manual){
        if (conn.isOnline()){
            if (manual){
                refresh.setRefreshing(true);
            }
            JSONObject requestObj = new JSONObject();
            try {
                requestObj.put("user_type_id", cache.getUserPref().getUserTypeId());
                requestObj.put("customer_id", cache.getUserPref().getId());
                requestObj.put("recent", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            requestQueue = Volley.newRequestQueue(getActivity());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.POST, dataSource.getOrdersUrl(), requestObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getBoolean("status")){
                            orderList.clear();

                            if (response.getJSONArray("data").length() != 0){
                                JSONArray objectArr = response.getJSONArray("data");
                                for (int i = 0; i < objectArr.length(); i++){
                                    CustomerOrder order = new CustomerOrder();
                                    order.setId(objectArr.getJSONObject(i).getString("id"));
                                    order.setOrderStatusId(objectArr.getJSONObject(i).getString("order_status_id"));
                                    order.setOrderStatusDesc(objectArr.getJSONObject(i).getString("order_status_desc"));
                                    order.setCustomerId(objectArr.getJSONObject(i).getString("customer_id"));
                                    order.setCustomerName(objectArr.getJSONObject(i).getString("customer_name"));
                                    order.setCustomerPhoneNum(objectArr.getJSONObject(i).getString("customer_phone_num"));
                                    order.setDriverId(objectArr.getJSONObject(i).getString("driver_id"));
                                    order.setDriverName(objectArr.getJSONObject(i).getString("driver_name"));
                                    order.setDriverPhoneNum(objectArr.getJSONObject(i).getString("driver_phone_num"));
                                    order.setCreatedAt(objectArr.getJSONObject(i).getString("created_at"));
                                    order.setOrderAcceptby(objectArr.getJSONObject(i).getString("order_accept_by"));
                                    orderList.add(order);
                                }
                                setupCustomerUI(orderList);
                            }
                            else{
                                hideCustomerUI();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hideCustomerUI();
                    }
                    refresh.setRefreshing(false);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    refresh.setRefreshing(false);
                    System.out.println(error);
                    hideCustomerUI();
                }
            }
            );

            requestQueue.add(jsonObjectRequest);

        }
        else{
            refresh.setRefreshing(false);
        }
    }

    public void getDriverData(Boolean manual){
        if (conn.isOnline()){
            if (manual){
                refresh.setRefreshing(true);
            }
            JSONObject requestObj = new JSONObject();
            try {
                requestObj.put("user_type_id", cache.getUserPref().getUserTypeId());
                requestObj.put("driver_id", cache.getUserPref().getId());
                requestObj.put("recent", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            requestQueue = Volley.newRequestQueue(getActivity());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.POST, dataSource.getOrdersUrl(), requestObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getBoolean("status")){
                            if (response.getJSONArray("data").length() != 0){
                                JSONObject obj = response.getJSONArray("data").getJSONObject(0);
                                CustomerOrder order = new CustomerOrder();
                                order.setId(obj.getString("id"));
                                order.setOrderStatusId(obj.getString("order_status_id"));
                                order.setOrderStatusDesc(obj.getString("order_status_desc"));
                                order.setCustomerId(obj.getString("customer_id"));
                                order.setCustomerName(obj.getString("customer_name"));
                                order.setCustomerPhoneNum(obj.getString("customer_phone_num"));
                                order.setDriverId(obj.getString("driver_id"));
                                order.setDriverName(obj.getString("driver_name"));
                                order.setDriverPhoneNum(obj.getString("driver_phone_num"));
                                order.setCreatedAt(obj.getString("created_at"));
                                order.setOrderAcceptby(obj.getString("order_accept_by"));
                                setupDriverUI(order);
                            }
                            else{
                                hideDriverUI();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hideDriverUI();
                    }
                    refresh.setRefreshing(false);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    refresh.setRefreshing(false);
                    System.out.println(error);
                    hideDriverUI();
                }
            }
            );

            requestQueue.add(jsonObjectRequest);

        }
        else{
            refresh.setRefreshing(false);
        }
    }

    public void hideDriverUI(){
        driverLayout.setVisibility(View.GONE);
        noRecentOrder.setVisibility(View.VISIBLE);
    }

    public void hideCustomerUI(){
        customerLayout.setVisibility(View.GONE);
        noRecentOrder.setVisibility(View.VISIBLE);
    }
}
