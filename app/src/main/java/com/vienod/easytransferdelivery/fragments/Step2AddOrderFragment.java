package com.vienod.easytransferdelivery.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.configs.ConnectionCheck;
import com.vienod.easytransferdelivery.configs.DataSource;
import com.vienod.easytransferdelivery.models.NewOrderData;
import com.vienod.easytransferdelivery.models.UserPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class Step2AddOrderFragment extends Fragment {

    private Cache cache;
    private TextView textTotalCost, textTotalDistance, textParcelSize;
    private Spinner paymentMethod;
    private LinearLayout cardMethodLayout;
    private Button buttonFinish;
    private EditText inputCardNumber, inputExpDate, inputCVV;
    private ProgressBar progressBar;
    private ConnectionCheck conn;
    private RequestQueue requestQueue;
    private DataSource dataSource;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_order_fragment_step_2,
                container, false);
    }

    public void onViewCreated (View view, Bundle savedInstanceState){
        textTotalCost = view.findViewById(R.id.text_total_cost);
        paymentMethod = view.findViewById(R.id.spinner_payment_method);
        cardMethodLayout = view.findViewById(R.id.non_cod_layout);
        buttonFinish = view.findViewById(R.id.button_finish);
        inputCardNumber = view.findViewById(R.id.input_card_number);
        inputExpDate = view.findViewById(R.id.input_expiry_date);
        inputCVV = view.findViewById(R.id.input_CVV);
        textTotalDistance = view.findViewById(R.id.text_total_distance);
        textParcelSize = view.findViewById(R.id.text_parcel_size);
        progressBar = view.findViewById(R.id.progress_bar);
        setup();
    }

    public void setup(){
        conn = new ConnectionCheck(getActivity());
        cache = new Cache(getActivity());
        dataSource = new DataSource();
        paymentMethod.setOnItemSelectedListener(methodListener);
        inputCardNumber.addTextChangedListener(validator);
        inputExpDate.addTextChangedListener(validator);
        buttonFinish.setOnClickListener(finish);
        inputCVV.addTextChangedListener(validator);
        if (cache.getNewOrderData() != null){
            textTotalCost.setText("RM " + cache.getNewOrderData().getOrderCost());
            textTotalDistance.setText(cache.getNewOrderData().getDistance() + " KM");
            textParcelSize.setText((cache.getNewOrderData().getParcelSizeDesc()).toUpperCase());
        }
        validateAllFields();
    }

    private View.OnClickListener finish = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            NewOrderData newOrderData = cache.getNewOrderData();
            newOrderData.setCardNumber(inputCardNumber.getText().toString().trim());
            newOrderData.setExpDate(inputExpDate.getText().toString().trim());
            newOrderData.setCvv(inputCVV.getText().toString().trim());

            if (paymentMethod.getSelectedItemId() == 0){
                newOrderData.setPaymentMethodId("1");
                newOrderData.setPaymentStatusId("1");
            }
            else{
                newOrderData.setPaymentMethodId("2");
                newOrderData.setPaymentStatusId("2");
            }

            if (cache.setNewOrderData(newOrderData)){
                if (conn.isOnline()){
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);

                    String pickupDateTime = cache.getNewOrderData().getPickupDateTime();
                    String pickupLocation = cache.getNewOrderData().getPickupLocation();
                    String dropoffDateTime = cache.getNewOrderData().getDropoffDateTime();
                    String dropoffLocation = cache.getNewOrderData().getDropoffLocation();
                    String customerId = cache.getUserPref().getId();
                    String paymentMethodId = cache.getNewOrderData().getPaymentMethodId();
                    String paymentStatusId = cache.getNewOrderData().getPaymentStatusId();
                    String cardNumber = cache.getNewOrderData().getCardNumber();
                    String totalCost = cache.getNewOrderData().getOrderCost();
                    String expiryDate = cache.getNewOrderData().getExpDate();
                    String cvv = cache.getNewOrderData().getCvv();
                    String parcelSizeId = cache.getNewOrderData().getParcelSizeId();
                    String deliveryDistance = cache.getNewOrderData().getDistance();

                    JSONObject requestObj = new JSONObject();
                    try {

                        requestObj.put("pickup_date_time", pickupDateTime);
                        requestObj.put("dropoff_date_time", dropoffDateTime);
                        requestObj.put("pickup_location", pickupLocation);
                        requestObj.put("dropoff_location", dropoffLocation);
                        requestObj.put("customer_id", customerId);
                        requestObj.put("payment_method_id", paymentMethodId);
                        requestObj.put("payment_status_id", paymentStatusId);
                        requestObj.put("total_cost_delivery", totalCost);
                        requestObj.put("card_number", cardNumber);
                        requestObj.put("expiry_date", expiryDate);
                        requestObj.put("cvv", cvv);
                        requestObj.put("delivery_distance", deliveryDistance);
                        requestObj.put("parcel_size_id", parcelSizeId);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    System.out.println(requestObj.toString());
                requestQueue = Volley.newRequestQueue(getActivity());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.POST, dataSource.getCustomerOrderCreationUrl(), requestObj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(getActivity(), response.getString("message"),
                                    Toast.LENGTH_SHORT).show();

                            if (response.getBoolean("status")){
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                                    fm.popBackStack();
                                    MainFragment mainFragment = new MainFragment();
                                    getActivity().getSupportFragmentManager()
                                            .beginTransaction()
//                                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                                            .replace(R.id.container, mainFragment).commit();
                                }
                            }
                            else{
                                progressBar.setVisibility(View.INVISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.INVISIBLE);
                        System.out.println(error);
                    }
                }
                );

                requestQueue.add(jsonObjectRequest);
                }

            }
        }
    };

    private AdapterView.OnItemSelectedListener methodListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0){
                cardMethodLayout.setVisibility(View.INVISIBLE);
            }
            else if (position == 1){
                cardMethodLayout.setVisibility(View.VISIBLE);
            }
            inputCardNumber.setText("");
            inputExpDate.setText("");
            inputCVV.setText("");
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private TextWatcher validator = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            validateAllFields();
        }
    };


    public void validateAllFields(){
        if (cardMethodLayout.getVisibility() == View.VISIBLE){
            String cardNumber = inputCardNumber.getText().toString().trim();
            String expDate = inputExpDate.getText().toString().trim();
            String cvv = inputCVV.getText().toString().trim();

            if (cardNumber.isEmpty() || expDate.isEmpty() || cvv.isEmpty()){
                buttonFinish.setVisibility(View.GONE);
            }
            else{
                buttonFinish.setVisibility(View.VISIBLE);
            }
        }
        else{
            buttonFinish.setVisibility(View.VISIBLE);
        }
    }
}
