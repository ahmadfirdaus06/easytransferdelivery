package com.vienod.easytransferdelivery.configs;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.vienod.easytransferdelivery.models.NewOrderData;
import com.vienod.easytransferdelivery.models.RegistrationData;
import com.vienod.easytransferdelivery.models.UserPref;

public class Cache {

    private Context context;

    public Cache(Context context) {
        this.context = context;
    }

    public RegistrationData getRegistrationData(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("REGISTRATION_DATA", Context.MODE_PRIVATE);
        String registrationData = sharedPreferences.getString("registration_data", "");
        Gson gson = new Gson();
        RegistrationData data = gson.fromJson(registrationData, RegistrationData.class);
        return data;
    }

    public boolean setRegistrationData(RegistrationData data){
        Gson gson = new Gson();
        String registrationData = gson.toJson(data);
        SharedPreferences sharedPreferences = context.getSharedPreferences("REGISTRATION_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("registration_data", registrationData);
        editor.commit();
        if (sharedPreferences.getString("registration_data", "") != ""){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean removeRegistrationData(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("REGISTRATION_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("registration_data");
        editor.commit();
        if (sharedPreferences.getString("registration_data", "") == ""){
            return true;
        }
        else{
            return false;
        }
    }

    public UserPref getUserPref(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER_PREF", Context.MODE_PRIVATE);
        String userPref = sharedPreferences.getString("user_pref", "");
        Gson gson = new Gson();
        UserPref data = gson.fromJson(userPref, UserPref.class);
        return data;
    }

    public boolean setUserPref(UserPref data){
        Gson gson = new Gson();
        String userPref = gson.toJson(data);
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER_PREF", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_pref", userPref);
        editor.commit();
        if (sharedPreferences.getString("user_pref", "") != ""){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean removeUserPref(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER_PREF", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("user_pref");
        editor.commit();
        if (sharedPreferences.getString("user_pref", "") == ""){
            return true;
        }
        else{
            return false;
        }
    }

    public NewOrderData getNewOrderData(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("NEW_ORDER_DATA", Context.MODE_PRIVATE);
        String newOrderData = sharedPreferences.getString("new_order_data", "");
        Gson gson = new Gson();
        NewOrderData data = gson.fromJson(newOrderData, NewOrderData.class);
        return data;
    }

    public boolean setNewOrderData(NewOrderData data){
        Gson gson = new Gson();
        String newOrderData = gson.toJson(data);
        SharedPreferences sharedPreferences = context.getSharedPreferences("NEW_ORDER_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("new_order_data", newOrderData);
        editor.commit();
        if (sharedPreferences.getString("new_order_data", "") != ""){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean removeNewOrderData(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("NEW_ORDER_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("new_order_data");
        editor.commit();
        if (sharedPreferences.getString("new_order_data", "") == ""){
            return true;
        }
        else{
            return false;
        }
    }
}
