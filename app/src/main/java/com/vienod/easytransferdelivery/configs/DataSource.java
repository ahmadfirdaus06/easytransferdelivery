package com.vienod.easytransferdelivery.configs;

public class DataSource {
//    private String ip = "10.23.33.160";
    private String ip = "192.168.1.208";
//    localhost
    private String host = "http://" + ip + "/easy-transfer-delivery-system-api";
//    server
//    private String host = "http://casual-server.ddns.net:49152/easy-transfer-delivery-system-api";
    private String companyDetails = host + "/api/user/get_company_details.php";
    private String registrationCheck = host + "/api/get/user_registration_check.php";
    private String staffJoinRequest = host + "/api/create/staff_join_request.php";
    private String customerJoin = host + "/api/create/customer_join.php";
    private String userLogin = host + "/api/get/mobile_user_login.php";
    private String parcelSize = host + "/api/get/parcel_size.php";
    private String customerOrderCreation = host + "/api/create/customer_order.php";
    private String orders = host + "/api/get/customer_order.php";
    private String orderUpdate = host + "/api/update/customer_order.php";
    private String userData = host + "/api/user/get_user_data.php";
    private String reports = host + "/api/get/report.php";

    public DataSource() {
    }

    public String getHost() {
        return host;
    }

    public String getCompanyUrl() {
        return companyDetails;
    }

    public String getRegistrationCheckUrl() {
        return registrationCheck;
    }

    public String getStaffJoinRequestUrl() {
        return staffJoinRequest;
    }

    public String getCustomerJoinUrl() {
        return customerJoin;
    }

    public String getUserLoginUrl() {
        return userLogin;
    }

    public String getParcelSizeUrl() {
        return parcelSize;
    }

    public String getCustomerOrderCreationUrl() {
        return customerOrderCreation;
    }

    public String getOrdersUrl() {
        return orders;
    }

    public String getOrderUpdateUrl() {
        return orderUpdate;
    }

    public String getUserData() {
        return userData;
    }

    public String getReports() {
        return reports;
    }
}
