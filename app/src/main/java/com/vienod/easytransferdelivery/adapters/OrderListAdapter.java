package com.vienod.easytransferdelivery.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.fragments.OrderDetailsFragment;
import com.vienod.easytransferdelivery.models.CustomerOrder;

import java.util.ArrayList;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ListViewHolder> {
    private ArrayList<CustomerOrder> orderList;
    private Context context;
    private OnItemClickListener listener;
    private static View list;
    private Cache cache;
    private int pageTag;

    public OrderListAdapter(Context context, ArrayList<CustomerOrder> orderList, int pageTag, OnItemClickListener listener) {
        this.orderList = orderList;
        this.context = context;
        this.listener = listener;
        this.pageTag = pageTag;
        cache = new Cache(context);
    }

    public interface OnItemClickListener {
        void onItemClick(CustomerOrder order);
    }

    public void refreshAdapter(ArrayList<CustomerOrder> orderList){
        this.orderList.clear();
        this.orderList = orderList;
        notifyDataSetChanged();
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (pageTag == 1){
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.orderlist_row_layout, viewGroup, false);
            list = itemView;
            return new ListViewHolder(itemView);
        }

        if (pageTag == 2){
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.orderlist_details_row_layout, viewGroup, false);
            list = itemView;
            return new ListViewHolder(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int i) {

        final CustomerOrder order = orderList.get(i);

        if (pageTag == 1){
            holder.textOrderId.setText(order.getId());
            holder.textCreatedAt.setText(order.getCreatedAt());
            if (cache.getUserPref().getUserTypeId().equals("2")){
                holder.miscCreatedAt.setText("Created on:");
                holder.textCurrent.setVisibility(View.GONE);
                if (Integer.parseInt(order.getOrderStatusId()) < 4){
                    holder.miscOrderId.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
                    holder.textOrderId.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
                    holder.miscCreatedAt.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    holder.textCreatedAt.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                }
                else{
                    holder.miscOrderId.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                    holder.textOrderId.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                    holder.miscCreatedAt.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                    holder.textCreatedAt.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray));
                }
            }
            else if (cache.getUserPref().getUserTypeId().equals("3")){

                holder.miscCreatedAt.setText("Available since:");
                if (!order.getDriverId().equals("null")){
                    holder.textCurrent.setVisibility(View.VISIBLE);
                }
                else{
                    holder.textCurrent.setVisibility(View.GONE);
                }
            }
            holder.bind(orderList.get(i), listener);
        }

        if (pageTag == 2){
            if (order.getDriverId().equals("null")){
                holder.textDriverName.setText("TBD");
                holder.textDriverPhoneNum.setText("TBD");
                holder.textOrderAcceptBy.setText("TBD");

            }
            else{
                holder.textDriverName.setText(order.getDriverName());
                holder.textDriverPhoneNum.setText(order.getDriverPhoneNum());
                holder.textOrderAcceptBy.setText(order.getOrderAcceptby());
            }
            holder.textOrderId.setText("Order #" + order.getId());
            holder.textOrderStatusDesc.setText(order.getOrderStatusDesc());
            holder.textCustomerName.setText(order.getCustomerName());
            holder.textCustomerPhoneNum.setText(order.getCustomerPhoneNum());
            holder.textCreatedAt.setText(order.getCreatedAt());
            holder.viewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("order_id", order.getId());
                    orderDetailsFragment.setArguments(bundle);
                    FragmentManager fm = ((AppCompatActivity)context).getSupportFragmentManager();
                    fm.beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                            .add(R.id.container, orderDetailsFragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }

        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }


    public static class ListViewHolder extends RecyclerView.ViewHolder {
        //tag = 1
        TextView textOrderId, textCreatedAt, miscOrderId, miscCreatedAt, textCurrent;

        //tag = 2
        TextView textOrderStatusDesc, textCustomerName, textCustomerPhoneNum, textDriverName,
                textDriverPhoneNum, textOrderAcceptBy;
        LinearLayout viewDetails;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            //tag = 1
            textOrderId = itemView.findViewById(R.id.text_order_id);
            textCreatedAt = itemView.findViewById(R.id.text_created_at);
            miscOrderId = itemView.findViewById(R.id.textView);
            miscCreatedAt = itemView.findViewById(R.id.textView2);
            textCurrent = itemView.findViewById(R.id.text_current);

            //tag = 2
            textOrderStatusDesc = itemView.findViewById(R.id.text_order_status_desc);
            textCustomerName = itemView.findViewById(R.id.text_customer_name);
            textCustomerPhoneNum = itemView.findViewById(R.id.text_customer_phone_num);
            textDriverName = itemView.findViewById(R.id.text_driver_name);
            textDriverPhoneNum = itemView.findViewById(R.id.text_driver_phone_num);
            textOrderAcceptBy = itemView.findViewById(R.id.text_accept_by);
            viewDetails = itemView.findViewById(R.id.info);
        }

        public void bind(final CustomerOrder order, final OnItemClickListener listener) {
            list.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(order);
                }
            });
        }
    }

}
