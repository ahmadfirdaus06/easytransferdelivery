package com.vienod.easytransferdelivery.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vienod.easytransferdelivery.R;
import com.vienod.easytransferdelivery.configs.Cache;
import com.vienod.easytransferdelivery.models.Report;

import java.util.ArrayList;

public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.ListViewHolder> {
    private ArrayList<Report> reportList;
    private Context context;
    private OnItemClickListener listener;
    private static View list;
    private Cache cache;

    public ReportListAdapter(Context context, ArrayList<Report> reportList, OnItemClickListener listener) {
        this.reportList = reportList;
        this.context = context;
        this.listener = listener;
        cache = new Cache(context);
    }

    public interface OnItemClickListener {
        void onItemClick(Report report);
    }

    public void refreshAdapter(ArrayList<Report> reportList){
        this.reportList.clear();
        this.reportList = reportList;
        notifyDataSetChanged();
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.reportlist_row_layout, viewGroup, false);
        list = itemView;
        return new ListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int i) {

        Report report = reportList.get(i);

        if (Float.parseFloat(report.getTotalRating()) == 0.0){
            holder.textTotalRating.setText("Not Rated");
        }
        else{
            holder.textTotalRating.setText(report.getTotalRating());
        }

        holder.textOrderId.setText(report.getCustomerOrderId());
        holder.textTotalEarned.setText("RM " + report.getTotalEarned());
        holder.textUpdatedAt.setText(report.getUpdatedAt());

        holder.bind(reportList.get(i), listener);
        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return reportList.size();
    }


    public static class ListViewHolder extends RecyclerView.ViewHolder {

        TextView textOrderId, textUpdatedAt, textTotalEarned, textTotalRating;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            textOrderId = itemView.findViewById(R.id.text_order_id);
            textUpdatedAt = itemView.findViewById(R.id.text_updated_at);
            textTotalEarned = itemView.findViewById(R.id.text_total_earned);
            textTotalRating = itemView.findViewById(R.id.text_total_rating);
        }

        public void bind(final Report report, final OnItemClickListener listener) {
            list.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(report);
                }
            });
        }
    }

}
